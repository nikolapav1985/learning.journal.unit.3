#!/usr/bin/python

"""
File count.py

EXAMPLES

INPUT 3

OUTPUT

Count value: 3
3
2
1
Blastoff!

INPUT -3

OUTPUT

Count value: -3
-3
-2
-1
Blastoff!

INPUT 0

OUTPUT

Count value: 0

Blastoff!

If count value is 0, any procedure can be called (countdn or countup). In this case, first available procedure is called, that is countdn.

"""

def countdn(n):
    """
        procedure countdn

        count in descending order e.g. 4 3 2 1 0

        parameters

        n(integer) initial value for count
    """
    if n<=0:
        print('Blastoff!')
    else:
        print(n)
        countdn(n-1)

def countup(n):
    """
        procedure countup

        count in ascending order e.g. -4 -3 -2 -1 0

        parameters

        n(integer) initial value for count
    """
    if n>=0:
        print('Blastoff!')
    else:
        print(n)
        countup(n+1)

if __name__=="__main__":
    item=int(raw_input("Count value: "))
    if item >=0:
        countdn(item)
    else:
        countup(item)
