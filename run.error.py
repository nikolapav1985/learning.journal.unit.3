#!/usr/bin/python

"""
FILE run.error.py

EXAMPLES

INPUT 5 3

OUTPUT 1

COMMENT

Program runs correctly and performs integer division.

INPUT 5 0

OUTPUT

Traceback (most recent call last):
  File "./run.error.py", line 30, in <module>
    print itema//itemb
ZeroDivisionError: integer division or modulo by zero

COMMENT

There is run time error message reporting that division by zero happened (line 36).
"""

if __name__=="__main__":
    itema=int(raw_input("Item a: "))
    itemb=int(raw_input("Item b: "))
    """
        This part is going to fail in case of division by zero (if itemb is zero). Possible fix A, do not enter zero for item b. There can be a message to indicate this. Possible fix B, do a conditional check. If itemb is zero exit program and print message.
    """
    print itema//itemb
