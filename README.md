Learning journal unit 3
------------------------

Files included  

- count.py (count descending and ascending using recursion example)
- run.error.py (example of run time error)

Check details
--------------

All details are found in comments in files count.py, run.error.py.

Run examples
------------

- type ./count.py
- type ./run.error.py

Test environment
----------------

OS lubuntu 16.04 lts 64 bit kernel version 4.13.0
